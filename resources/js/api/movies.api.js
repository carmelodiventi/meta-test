import * as types from '../actions/actionTypes';
import { config } from '../constants/config.constants';
import Axios from 'axios';
 
function dispatchSuccess(type,response){
    return {
        type: type,
        payload: response
    }
}

function dispatchError(type,response){
    return{
        type: type,
        payload: response
    }
}


export function getData(term){

    const url = `${config.OMDB_API_URL}/?s=${encodeURIComponent(term.trim())}&apikey=${config.API_KEY}`;

    return function(dispatch){
        
        dispatch(dispatchSuccess(types.REQUEST_MOVIES_DETAILS,null));

        fetch(url)
        .then(response => response.json())
        .then(data =>  {
            if(data && data.Response){
                dispatch(dispatchSuccess(types.GET_MOVIES_DETAILS,data)) 
            }
            else{
                dispatch(dispatchSuccess(types.GET_MOVIES_DETAILS_FAIL,data)) 
            }
        });

    }

    return false;

}

export function storeData(data){

    const url = `${config.API_URL}/store-data`;

    axios.post(url,{data}).then( response => {
        if(response.data && response.data.success){
            console.log('data stored');
        }
    });

}