import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import Movies from '../containers/Movies';
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

export default class App extends Component {

    render() {
        return (
            <div>
            <header>
                <div className="collapse bg-dark" id="navbarHeader">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12">
                                <h4 className="text-white">Meta Test</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="navbar navbar-dark bg-dark shadow-sm">
                    <div className="container d-flex justify-content-between">
                        <a href="#" className="navbar-brand d-flex align-items-center">
                            <strong>Meta Test</strong>
                        </a>
                    </div>
                </div>
            </header>
            <main role="main">
                <Movies/>
            </main>
            </div>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <App />
    </Provider>
    ,document.getElementById('app'));
}
