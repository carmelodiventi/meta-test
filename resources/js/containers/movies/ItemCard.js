import React from 'react';

export default function ItemCard(props){

        let poster = props.details.Poster;

        if(poster && poster != 'N/A'){
            poster = {backgroundImage: `url(${poster})`};
        }
        else{
            poster = {backgroundColor: `#ccc`};
        }

        return(
            <div className="col-md-4 col-sm-12">
               <div className="card">
                    <div className="card-img" style={poster}></div>
                    <div className="card-body">
                        {props.details.Title &&
                            <h5> {props.details.Title} </h5>
                        }
                        <div> 
                            {props.details.Type && <strong>Type: {props.details.Type}</strong>} 
                            &nbsp;
                            {props.details.Year && <strong>Year: {props.details.Year}</strong>} 
                        </div>
                    </div>
               </div>
            </div>
        )

}