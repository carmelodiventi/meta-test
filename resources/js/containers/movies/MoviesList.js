import React, {Component} from 'react';
import ItemCard from './ItemCard';

export default class MovisList extends Component{

    constructor(props){
        super(props);
    }

    render(){

        let items = this.props.list.map( (item, index) => <ItemCard key={index} details={item} /> );

        return(
            <div className="container">
                {this.props.isDispatching &&
                    <div className="text-center">
                        Processing Data...
                    </div>
                }
                <div className="row">
                        {items}
                </div>
            </div>
        )

    }

}