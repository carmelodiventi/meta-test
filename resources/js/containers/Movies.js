import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { getData,storeData } from "../api/movies.api";
import MoviesList from '../containers/movies/MoviesList';

const buttons = [
    {name : 'Matrix'},
    {name : 'Matrix Reloaded'},
    {name : 'Matrix Revolutions'}
]

class Movies extends Component {

    constructor(props){
        super(props);
        this.getItemDetails = this.getItemDetails.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.movies.list !== this.props.movies.list){
            storeData(nextProps.movies.list);
        }
    }

    getItemDetails(movie){
        this.props.getData(movie);
    }

    render(){

        let buttonsList = buttons.map( (item, index) => <button key={index} className='btn btn-primary' onClick={() => this.getItemDetails(item.name)}> {item.name} </button>);

        return (
            <div>
                <section className="jumbotron text-center">
                    <div className="container">
                        <h1 className="jumbotron-heading">Movies example</h1>
                        <p className="lead text-muted">Discovering a list of unforgettable movies.</p>
                        <div className="btn-list">
                            {buttonsList}
                        </div>
                    </div>
                </section>
                <MoviesList list={this.props.movies.list} isDispatching={this.props.isDispatching} />
            </div>
        )
    }

}

function mapDispatchToProps(dispatch) {  
    return bindActionCreators({ getData }, dispatch);
}
  
function mapStateToProps(state){
    return {
        movies : state.movies
    }
}
  
export default connect(mapStateToProps,mapDispatchToProps)(Movies);
