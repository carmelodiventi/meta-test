import { REQUEST_MOVIES_DETAILS, GET_MOVIES_DETAILS, GET_MOVIES_DETAILS_FAIL } from '../actions/actionTypes';

const initialState = {
    list : [],
    isDispatching : false
}

export default function (state = initialState,action){

    switch (action.type) {

        case REQUEST_MOVIES_DETAILS :

        return {
            ...state,
            isDispatching : true
        }

        case GET_MOVIES_DETAILS :

        return {
            ...state,
            list : action.payload.Search,
            isDispatching:false
        }

        case GET_MOVIES_DETAILS_FAIL :

        return {
            ...state,
            isDispatching : false
        }

        default :
        return state;

    }

}