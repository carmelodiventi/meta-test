<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Meta Test</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
    <main id="app" role="main"></main>
    <script src="{{asset('js/app.js')}}" ></script>
</body>
</html>
