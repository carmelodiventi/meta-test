## Meta Test

In this application test I have used Laravel as backend to store the data fetched from the urls provided, supposing that the list was created from a first call, I have stored the url list on an array in a React controller and showed as buttons, the buttons clicked call a function where will fetch the data from an api url and will update the state with the result, I have used redux to manage the app state, was also possible use only the React state due to the app functionality, but I think that spend some time in the begining of a app's creation and the configuration of the structure will increase the possibility to improve the app management in the next update. 

- Database creation: create a database called 'meta_test' and lunch on the console 'php artisan migrate'
- Laravel Setup: composer install and rename file .env.example to .env
- App Setup: npm install and npm run prod
- Nginx Setup: add the configuration file metatest.conf on the site-available's folder and update the root path as needed


- The app is available on the public folder of Laravel, was also possible create two different app Laravel and a Js App that was calling the api system, but I prefered use all in a packet. 
- All app's files are available on the resources's folder.

Thanks, and sorry for my English