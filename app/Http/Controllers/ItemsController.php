<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Type;
use App\Item;

class ItemsController extends Controller
{
    //

    public function storeData(Request $request){

        $validator = Validator::make($request->all(),
            ['data' => 'required']
        );

        if ($validator->fails()) {
            $result = ['success' => false, 'errors' => $validator->errors()];
            return response()->json($result);
        }

        $items = $request->data;
    
           
        foreach($items as $item){

            $type = Type::firstOrCreate([
                'type'   => $item['Type'],
            ],[
                'type'     => $item['Type']
            ]);

            $newItem = Item::firstOrCreate([
                'imdbID'   => $item['imdbID'],
            ],[
                'title' => $item['Title'],
                'type'     => $type->id,
                'poster'     => $item['Poster']
            ]);

        }

        $result = [
            'success' => true
        ];
        

        return response()->json($result);

    }

}
